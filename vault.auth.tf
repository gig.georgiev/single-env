resource "vault_jwt_auth_backend_role" "auth" {
  backend                 = vault_auth_backend.auth_jwt.path
  bound_claims            = local.jwt_bound_claims
  role_name               = local.auth_role
  role_type               = "jwt"
  token_explicit_max_ttl  = 60
  token_no_default_policy = false
  token_policies = [
    vault_policy.policy.name
  ]
  user_claim = "user_email"
}
