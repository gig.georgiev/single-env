resource "vault_transit_secret_backend_key" "transit" {
  for_each = {
    for obj in local.transit_keys : obj.name => obj
  }

  backend          = "transit"
  name             = each.value.name
  deletion_allowed = each.value.deletion_allowed
}
