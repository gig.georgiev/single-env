output "bound_claims" {
  value = vault_jwt_auth_backend_role.auth.bound_claims
}

output "dev_approle" {
  value = var.enable_dev_approle
}

output "policies" {
  value = vault_policy.policy.policy
}

output "role_name" {
  value = vault_jwt_auth_backend_role.auth.role_name
}
