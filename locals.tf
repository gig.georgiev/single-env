locals {
  // figure vars to use
  env              = var.env
  auth_role        = "${var.project_name}_${var.env}"
  policy_name      = "${var.project_name}_${var.env}"
  jwt_bound_claims = merge(var.jwt_bound_claims, { project_id : var.gitlab_vault_jwt_project_id })
  policy_rules = concat(
    flatten([
      for transit_key in var.transit_keys : [
        for transit_dir in ["encrypt", "decrypt"] : {
          path         = "transit/${transit_dir}/${var.project_name}_${var.env}_${transit_key.name}"
          capabilities = ["update"]
          description  = "${transit_dir} using key ${transit_key.name}. ${transit_key.description}"
        }
      ]
    ]),
    [
      for read_secret in var.read_secrets : {
        path         = "kv/data/${var.project_name}/${var.env}/${read_secret.name}"
        capabilities = ["read"]
        description  = read_secret.description
      }
    ],
  )
  transit_keys = flatten([
    for transit_key in var.transit_keys : {
      name             = "${var.project_name}_${var.env}_${transit_key.name}"
      deletion_allowed = transit_key.deletion_allowed
    }
  ])

  // validate policies
  validation = (
    length(var.transit_keys) + length(var.read_secrets)
    ) == 0 ? file(<<EOT
  [Error] At least 1 transit_keys or read_secrets must be used.
  (no purpose for lib otherwise)
  EOT
  ) : true
}
