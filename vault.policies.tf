resource "vault_policy" "policy" {
  name   = local.policy_name
  policy = data.vault_policy_document.policy.hcl
}

data "vault_policy_document" "policy" {
  // TODO remove/adjust/template this policy once we can remove it from direct JWT config
  dynamic "rule" {
    for_each = local.policy_rules
    content {
      capabilities = rule.value.capabilities
      description  = rule.value.description
      path         = rule.value.path
    }
  }
}
