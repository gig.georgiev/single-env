data "vault_auth_backend" "auth_jwt" {
  path = var.backend_jwt
}

data "vault_auth_backend" "auth_approle" {
  path = var.backend_approle
}
